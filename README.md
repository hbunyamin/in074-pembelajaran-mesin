# The Repo for IN074 Pembelajaran Mesin at Maranatha Christian University

All the codes are modification from codes from the book **Hands-On Machine Learning with Scikit-Learn, Keras & TensorFlow** by Aurelien Geron.     
      
Please buy the book to respect the author. Thank you!

Specifically, to run the codes we can install the conda environment as follows:    
> $ conda env create -f environment.yaml     
     
Enjoy!
